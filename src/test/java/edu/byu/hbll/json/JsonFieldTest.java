package edu.byu.hbll.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JsonFieldTest {

  private static ObjectMapper mapper = new ObjectMapper();

  private ObjectNode root;

  @BeforeEach
  public void setUp() {
    root = mapper.createObjectNode();

    // add object
    root.with("object").put("a", "x").put("b", "y").put("c", "z");

    // add array
    root.withArray("array").add("q").add("r").add("s");

    // add value node
    root.put("value", "v");
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.JsonField#JsonField(com.fasterxml.jackson.databind.JsonNode)}.
   */
  @Test
  public void testJsonFieldJsonNode() {
    JsonNode node = mapper.createObjectNode();
    JsonField field = new JsonField(node);

    assertEquals("0", field.getKey());
    assertEquals(node, field.getValue());
  }

  /**
   * Test method for {@link edu.byu.hbll.json.JsonField#JsonField(java.lang.String,
   * com.fasterxml.jackson.databind.JsonNode)}.
   */
  @Test
  public void testJsonFieldStringJsonNode() {
    JsonNode node = mapper.createObjectNode();
    JsonField field = new JsonField("1", node);

    assertEquals("1", field.getKey());
    assertEquals(node, field.getValue());
  }

  /** Test method for {@link edu.byu.hbll.json.JsonField#fieldNames()}. */
  @Test
  public void testFieldNames() {
    List<String> fieldNames = new JsonField(root.path("object")).fieldNames();

    assertEquals("a", fieldNames.get(0));
    assertEquals("b", fieldNames.get(1));
    assertEquals("c", fieldNames.get(2));

    fieldNames = new JsonField(root.path("array")).fieldNames();

    assertEquals("0", fieldNames.get(0));
    assertEquals("1", fieldNames.get(1));
    assertEquals("2", fieldNames.get(2));

    fieldNames = new JsonField(root.path("value")).fieldNames();

    assertEquals("0", fieldNames.get(0));

    fieldNames = new JsonField(root.path("missing")).fieldNames();

    assertTrue(fieldNames.isEmpty());
  }

  /** Test method for {@link edu.byu.hbll.json.JsonField#fields()}. */
  @Test
  public void testFields() {
    List<JsonField> fields = new JsonField(root.path("object")).fields();

    assertEquals("a", fields.get(0).getKey());
    assertEquals(root.path("object").path("a"), fields.get(0).getValue());
    assertEquals("b", fields.get(1).getKey());
    assertEquals(root.path("object").path("b"), fields.get(1).getValue());
    assertEquals("c", fields.get(2).getKey());
    assertEquals(root.path("object").path("c"), fields.get(2).getValue());

    fields = new JsonField(root.path("array")).fields();

    assertEquals("0", fields.get(0).getKey());
    assertEquals(root.path("array").path(0), fields.get(0).getValue());
    assertEquals("1", fields.get(1).getKey());
    assertEquals(root.path("array").path(1), fields.get(1).getValue());
    assertEquals("2", fields.get(2).getKey());
    assertEquals(root.path("array").path(2), fields.get(2).getValue());

    fields = new JsonField(root.path("value")).fields();

    assertEquals("0", fields.get(0).getKey());
    assertEquals(root.path("value"), fields.get(0).getValue());

    fields = new JsonField(root.path("missing")).fields();

    assertTrue(fields.isEmpty());

    root.putNull("null");
    fields = new JsonField(root.path("null")).fields();

    assertTrue(fields.isEmpty());
  }

  /** Test method for {@link edu.byu.hbll.json.JsonField#toString()}. */
  @Test
  public void testToString() {
    assertEquals("\"a\\n\\\"\":" + root, new JsonField("a\n\"", root).toString());
  }

  /** Test method for {@link edu.byu.hbll.json.JsonField#iterator()}. */
  @Test
  public void testIterator() {
    List<String> fieldNames = new ArrayList<>(Arrays.asList("object", "array", "value"));

    for (JsonField field : new JsonField(root)) {
      assertEquals(root.path(fieldNames.remove(0)), field.getValue());
    }
  }

  /** Test method for {@link edu.byu.hbll.json.JsonField#stream()}. */
  @Test
  public void testStream() {
    List<JsonNode> fields =
        new JsonField(root).stream().map(JsonField::getValue).collect(Collectors.toList());
    Iterator<JsonNode> it = root.iterator();

    for (int i = 0; i < 3; i++) {
      assertEquals(it.next(), fields.get(i));
    }
  }
}
