package edu.byu.hbll.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.StringReader;
import org.junit.jupiter.api.Test;

/**
 * Tests for {@link JsonUtils}.
 *
 * @author Charles Draper
 */
public class JsonUtilsTest {

  private static String simpleFieldSourceA = "---" + System.lineSeparator() + "key : a";

  private static String simpleFieldSourceB = "---" + System.lineSeparator() + "key : b";

  private static String simpleFieldSourceNull = "---" + System.lineSeparator() + "key : null";

  private static String arraySourceAB =
      "---"
          + System.lineSeparator()
          + "- elementA"
          + System.lineSeparator()
          + "- elementB"
          + System.lineSeparator();

  private static String arraySourceC = "---" + System.lineSeparator() + "- elementC";

  private static String objectSourceAB =
      "---"
          + System.lineSeparator()
          + "key: "
          + System.lineSeparator()
          + "  a : alpha"
          + System.lineSeparator()
          + "  b : bravo";

  private static String objectSourceAC =
      "---"
          + System.lineSeparator()
          + "key : "
          + System.lineSeparator()
          + "  a : new alpha"
          + System.lineSeparator()
          + "  c : charlie";

  private static YamlLoader loader = new YamlLoader();

  /**
   * The merge method should fully overwrite/replace a simple field with a null value. Since the
   * null value is itself a simple field, if this test passes, it is assumed that overwriting an
   * array and a complex object will also work assuming that they can be overwritten by a simple
   * field.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void merge_shouldOverwriteFieldWithNull() throws Exception {
    JsonNode a = loader.load(new StringReader(simpleFieldSourceA));
    JsonNode b = loader.load(new StringReader(simpleFieldSourceNull));

    JsonNode result = JsonUtils.merge(a, b);
    assertNull(result.get("key").asText(null));
  }

  /**
   * The merge method should fully overwrite/replace a simple field with any other simple field.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void merge_shouldOverwriteFieldWithOtherField() throws Exception {
    JsonNode a = loader.load(new StringReader(simpleFieldSourceA));
    JsonNode b = loader.load(new StringReader(simpleFieldSourceB));

    JsonNode result = JsonUtils.merge(a, b);
    assertEquals("b", result.get("key").asText());
  }

  /**
   * The merge method should fully overwrite/replace a simple field with an array.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void merge_shouldOverwriteFieldWithArray() throws Exception {
    JsonNode a = loader.load(new StringReader(simpleFieldSourceA));
    JsonNode b = loader.load(new StringReader(arraySourceAB));

    JsonNode result = JsonUtils.merge(a, b);
    assertTrue(result.isArray());
    assertEquals("elementA", result.get(0).asText(null));
    assertEquals("elementB", result.get(1).asText(null));
    assertNull(result.get(2));
  }

  /**
   * The merge method should fully overwrite/replace a simple field with a complex object.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void merge_shouldOverwriteFieldWithObject() throws Exception {
    JsonNode a = loader.load(new StringReader(simpleFieldSourceA));
    JsonNode b = loader.load(new StringReader(objectSourceAB));

    JsonNode result = JsonUtils.merge(a, b);
    assertTrue(result.get("key").isObject());
    assertEquals("alpha", result.get("key").get("a").asText());
    assertEquals("bravo", result.get("key").get("b").asText());
  }

  /**
   * The merge method should fully overwrite/replace an array with a simple field.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void merge_shouldOverwriteArrayWithField() throws Exception {
    JsonNode a = loader.load(new StringReader(arraySourceAB));
    JsonNode b = loader.load(new StringReader(simpleFieldSourceA));

    JsonNode result = JsonUtils.merge(a, b);
    assertEquals("a", result.get("key").asText());
    assertNull(result.get(1));
  }

  /**
   * The merge method should fully overwrite/replace an array with another array.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void merge_shouldOverwriteArrayWithOtherArray() throws Exception {
    JsonNode a = loader.load(new StringReader(arraySourceAB));
    JsonNode b = loader.load(new StringReader(arraySourceC));

    JsonNode result = JsonUtils.merge(a, b);
    assertTrue(result.isArray());
    assertEquals("elementC", result.get(0).asText());
    assertNull(result.get(1));
  }

  /**
   * The merge method should fully overwrite/replace an array with a complex object.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void merge_shouldOverwriteArrayWithObject() throws Exception {
    JsonNode a = loader.load(new StringReader(arraySourceAB));
    JsonNode b = loader.load(new StringReader(objectSourceAB));

    JsonNode result = JsonUtils.merge(a, b);
    assertTrue(result.get("key").isObject());
    assertEquals("alpha", result.get("key").get("a").asText());
    assertEquals("bravo", result.get("key").get("b").asText());
  }

  /**
   * The merge method should fully overwrite/replace a complex object with a simple field.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void merge_shouldOverwriteObjectWithField() throws Exception {
    JsonNode a = loader.load(new StringReader(objectSourceAB));
    JsonNode b = loader.load(new StringReader(simpleFieldSourceA));

    JsonNode result = JsonUtils.merge(a, b);
    assertFalse(result.get("key").isObject());
    assertEquals("a", result.get("key").asText());
  }

  /**
   * The merge method should fully overwrite/replace a complex object with an array.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void merge_shouldOverwriteObjectWithArray() throws Exception {
    JsonNode a = loader.load(new StringReader(objectSourceAB));
    JsonNode b = loader.load(new StringReader(arraySourceC));

    JsonNode result = JsonUtils.merge(a, b);
    assertTrue(result.isArray());
    assertEquals("elementC", result.get(0).asText());
    assertNull(result.get(1));
  }

  /**
   * The merge method should recursively merge two complex objects.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void merge_shouldMergeTwoObjects() throws Exception {
    JsonNode a = loader.load(new StringReader(objectSourceAB));
    JsonNode b = loader.load(new StringReader(objectSourceAC));

    JsonNode result = JsonUtils.merge(a, b);
    assertEquals("new alpha", result.path("key").path("a").asText());
    assertEquals("bravo", result.path("key").path("b").asText());
    assertEquals("charlie", result.path("key").path("c").asText());
  }

  /**
   * Null values and missing nodes are ignored in argument list.
   *
   * @throws Exception
   */
  @Test
  public void merge_nullsAndMissingIgnored() throws Exception {
    JsonNode a = loader.load(new StringReader(objectSourceAB));
    JsonNode b = loader.load(new StringReader(objectSourceAC));

    JsonNode result = JsonUtils.merge(null, a, null, b, a.path("missing"), null);
    assertEquals("new alpha", result.path("key").path("a").asText());
    assertEquals("bravo", result.path("key").path("b").asText());
    assertEquals("charlie", result.path("key").path("c").asText());
  }

  /**
   * {@link NullNode}'s should overwrite objects just like any other value node.
   *
   * @throws Exception
   */
  @Test
  public void merge_shouldOverwriteObjectWithNull() throws Exception {
    JsonNode a = loader.load(new StringReader(objectSourceAB));
    JsonNode b = loader.load(new StringReader(objectSourceAC));
    ((ObjectNode) b).putNull("null");

    JsonNode result = JsonUtils.merge(a, b.path("null"));
    assertTrue(result.isNull());
  }

  /** {@link JsonUtils#fixArrays(JsonNode)} should properly convert maps into arrays. */
  @Test
  public void testFixArrays() throws Exception {
    JsonNode arrays = loader.loadPaths("src/test/resources/arrays-test.yml");
    JsonUtils.fixArrays(arrays);
    assertEquals(3, arrays.path("config").path("uris").size());
    assertTrue(arrays.path("config").path("uris").isArray());
    assertEquals("a", arrays.path("config").path("uris").get(0).asText());
    assertEquals("b", arrays.path("config").path("uris").get(1).asText());
    assertEquals("c", arrays.path("config").path("uris").get(2).asText());
  }
}
