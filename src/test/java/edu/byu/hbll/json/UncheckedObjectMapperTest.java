package edu.byu.hbll.json;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.BeanDeserializerFactory;
import com.fasterxml.jackson.databind.deser.DefaultDeserializationContext;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatVisitorWrapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UncheckedIOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;

/**
 * @author Charles Draper
 */
class UncheckedObjectMapperTest {

  private ObjectMapper mapper = new ObjectMapper();
  private UncheckedObjectMapper umapper = new UncheckedObjectMapper();

  private Sample sample = new Sample("1", 2);
  private JsonNode node = mapper.convertValue(sample, JsonNode.class);
  private String text = node.toString();
  private byte[] bytes = text.getBytes();
  private byte[] extraBytes = (text + "{").getBytes();
  private TypeReference<Sample> ref = new TypeReference<Sample>() {};
  private JavaType type = mapper.constructType(Sample.class);

  /** Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#UncheckedObjectMapper()}. */
  @Test
  void testUncheckedObjectMapper() {
    new UncheckedObjectMapper().createObjectNode();
    assertEquals("1", sample.getA());
    assertEquals(2, sample.getB());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#UncheckedObjectMapper(com.fasterxml.jackson.core.JsonFactory)}.
   */
  @Test
  void testUncheckedObjectMapperJsonFactory() {
    new UncheckedObjectMapper(new JsonFactory()).createObjectNode();
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#UncheckedObjectMapper(com.fasterxml.jackson.core.JsonFactory,
   * com.fasterxml.jackson.databind.ser.DefaultSerializerProvider,
   * com.fasterxml.jackson.databind.deser.DefaultDeserializationContext)}.
   */
  @Test
  void
      testUncheckedObjectMapperJsonFactoryDefaultSerializerProviderDefaultDeserializationContext() {
    new UncheckedObjectMapper(
            new JsonFactory(),
            new DefaultSerializerProvider.Impl(),
            new DefaultDeserializationContext.Impl(BeanDeserializerFactory.instance))
        .createObjectNode();
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#UncheckedObjectMapper(com.fasterxml.jackson.databind.ObjectMapper)}.
   */
  @Test
  void testUncheckedObjectMapperObjectMapper() {
    new UncheckedObjectMapper(mapper).createObjectNode();
  }

  /** Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#copy()}. */
  @Test
  void testCopy() {
    umapper.copy().createObjectNode();
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(byte[],
   * java.lang.Class)}.
   */
  @Test
  void testReadValueByteArrayClassOfT() throws IOException {
    assertEquals(mapper.readValue(bytes, Sample.class), umapper.readValue(bytes, Sample.class));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(byte[], int, int,
   * java.lang.Class)}.
   */
  @Test
  void testReadValueByteArrayIntIntClassOfT() throws IOException {
    assertEquals(
        mapper.readValue(extraBytes, 0, text.length(), Sample.class),
        umapper.readValue(extraBytes, 0, text.length(), Sample.class));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(byte[],
   * com.fasterxml.jackson.core.type.TypeReference)}.
   */
  @Test
  void testReadValueByteArrayTypeReferenceOfT() throws IOException {
    assertEquals(mapper.readValue(bytes, ref), umapper.readValue(bytes, ref));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(byte[], int, int,
   * com.fasterxml.jackson.core.type.TypeReference)}.
   */
  @Test
  void testReadValueByteArrayIntIntTypeReferenceOfT() throws IOException {
    assertEquals(
        mapper.readValue(extraBytes, 0, text.length(), ref),
        umapper.readValue(extraBytes, 0, text.length(), ref));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(byte[],
   * com.fasterxml.jackson.databind.JavaType)}.
   */
  @Test
  void testReadValueByteArrayJavaType() throws IOException {
    assertEquals((Sample) mapper.readValue(bytes, type), (Sample) umapper.readValue(bytes, type));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(byte[], int, int,
   * com.fasterxml.jackson.databind.JavaType)}.
   */
  @Test
  void testReadValueByteArrayIntIntJavaType() throws IOException {
    assertEquals(
        (Sample) mapper.readValue(extraBytes, 0, text.length(), type),
        (Sample) umapper.readValue(extraBytes, 0, text.length(), type));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.io.DataInput,
   * java.lang.Class)}.
   */
  @Test
  void testReadValueDataInputClassOfT() throws IOException {
    assertEquals(
        mapper.readValue(dataInput(), Sample.class), umapper.readValue(dataInput(), Sample.class));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.io.DataInput,
   * com.fasterxml.jackson.databind.JavaType)}.
   */
  @Test
  void testReadValueDataInputJavaType() throws IOException {
    assertEquals(
        (Sample) mapper.readValue(dataInput(), type),
        (Sample) umapper.readValue(dataInput(), type));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.io.File,
   * java.lang.Class)}.
   */
  @Test
  void testReadValueFileClassOfT() throws IOException {
    assertEquals(mapper.readValue(file(), Sample.class), umapper.readValue(file(), Sample.class));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.io.File,
   * com.fasterxml.jackson.core.type.TypeReference)}.
   */
  @Test
  void testReadValueFileTypeReferenceOfT() throws IOException {
    assertEquals(mapper.readValue(file(), ref), umapper.readValue(file(), ref));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.io.File,
   * com.fasterxml.jackson.databind.JavaType)}.
   */
  @Test
  void testReadValueFileJavaType() throws IOException {
    assertEquals((Sample) mapper.readValue(file(), type), (Sample) umapper.readValue(file(), type));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.net.URL,
   * java.lang.Class)}.
   */
  @Test
  void testReadValueURLClassOfT() throws IOException {
    assertEquals(mapper.readValue(url(), Sample.class), umapper.readValue(url(), Sample.class));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.net.URL,
   * com.fasterxml.jackson.core.type.TypeReference)}.
   */
  @Test
  void testReadValueURLTypeReferenceOfT() throws IOException {
    assertEquals(mapper.readValue(url(), ref), umapper.readValue(url(), ref));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.net.URL,
   * com.fasterxml.jackson.databind.JavaType)}.
   */
  @Test
  void testReadValueURLJavaType() throws IOException {
    assertEquals((Sample) mapper.readValue(url(), type), (Sample) umapper.readValue(url(), type));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.lang.String,
   * java.lang.Class)}.
   */
  @Test
  void testReadValueStringClassOfT() throws IOException {
    assertEquals(mapper.readValue(text, Sample.class), umapper.readValue(text, Sample.class));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.lang.String,
   * com.fasterxml.jackson.core.type.TypeReference)}.
   */
  @Test
  void testReadValueStringTypeReferenceOfT() throws IOException {
    assertEquals(mapper.readValue(text, ref), umapper.readValue(text, ref));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.lang.String,
   * com.fasterxml.jackson.databind.JavaType)}.
   */
  @Test
  void testReadValueStringJavaType() throws IOException {
    assertEquals((Sample) mapper.readValue(text, type), (Sample) umapper.readValue(text, type));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.io.Reader,
   * java.lang.Class)}.
   */
  @Test
  void testReadValueReaderClassOfT() throws IOException {
    assertEquals(
        mapper.readValue(reader(), Sample.class), umapper.readValue(reader(), Sample.class));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.io.Reader,
   * com.fasterxml.jackson.core.type.TypeReference)}.
   */
  @Test
  void testReadValueReaderTypeReferenceOfT() throws IOException {
    assertEquals(mapper.readValue(reader(), ref), umapper.readValue(reader(), ref));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.io.Reader,
   * com.fasterxml.jackson.databind.JavaType)}.
   */
  @Test
  void testReadValueReaderJavaType() throws IOException {
    assertEquals(
        (Sample) mapper.readValue(reader(), type), (Sample) umapper.readValue(reader(), type));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.io.InputStream,
   * java.lang.Class)}.
   */
  @Test
  void testReadValueInputStreamClassOfT() throws IOException {
    assertEquals(
        mapper.readValue(inputStream(), Sample.class),
        umapper.readValue(inputStream(), Sample.class));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.io.InputStream,
   * com.fasterxml.jackson.core.type.TypeReference)}.
   */
  @Test
  void testReadValueInputStreamTypeReferenceOfT() throws IOException {
    assertEquals(mapper.readValue(inputStream(), ref), umapper.readValue(inputStream(), ref));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readValue(java.io.InputStream,
   * com.fasterxml.jackson.databind.JavaType)}.
   */
  @Test
  void testReadValueInputStreamJavaType() throws IOException {
    assertEquals(
        (Sample) mapper.readValue(inputStream(), type),
        (Sample) umapper.readValue(inputStream(), type));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#readValue(com.fasterxml.jackson.core.JsonParser,
   * java.lang.Class)}.
   */
  @Test
  void testReadValueJsonParserClassOfT() throws IOException {
    assertEquals(
        mapper.readValue(jsonParser(), Sample.class),
        umapper.readValue(jsonParser(), Sample.class));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#readValue(com.fasterxml.jackson.core.JsonParser,
   * com.fasterxml.jackson.core.type.TypeReference)}.
   */
  @Test
  void testReadValueJsonParserTypeReferenceOfT() throws IOException {
    assertEquals(mapper.readValue(jsonParser(), ref), umapper.readValue(jsonParser(), ref));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#readValue(com.fasterxml.jackson.core.JsonParser,
   * com.fasterxml.jackson.databind.JavaType)}.
   */
  @Test
  void testReadValueJsonParserJavaType() throws IOException {
    assertEquals(
        (Sample) mapper.readValue(jsonParser(), type),
        (Sample) umapper.readValue(jsonParser(), type));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#readTree(com.fasterxml.jackson.core.JsonParser)}.
   */
  @Test
  void testReadTreeJsonParser() throws IOException {
    assertEquals(
        (ObjectNode) mapper.readTree(jsonParser()), (ObjectNode) umapper.readTree(jsonParser()));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readTree(java.io.InputStream)}.
   */
  @Test
  void testReadTreeInputStream() throws IOException {
    assertEquals(mapper.readTree(inputStream()), umapper.readTree(inputStream()));
  }

  /** Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readTree(java.io.Reader)}. */
  @Test
  void testReadTreeReader() throws IOException {
    assertEquals(mapper.readTree(reader()), umapper.readTree(reader()));
  }

  /** Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readTree(java.lang.String)}. */
  @Test
  void testReadTreeString() throws IOException {
    assertEquals(mapper.readTree(text), umapper.readTree(text));
  }

  /** Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readTree(byte[])}. */
  @Test
  void testReadTreeByteArray() throws IOException {
    assertEquals(mapper.readTree(bytes), umapper.readTree(bytes));
  }

  /** Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readTree(byte[], int, int)}. */
  @Test
  void testReadTreeByteArrayIntInt() throws IOException {
    assertEquals(
        mapper.readTree(extraBytes, 0, bytes.length),
        umapper.readTree(extraBytes, 0, bytes.length));
  }

  /** Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readTree(java.io.File)}. */
  @Test
  void testReadTreeFile() throws IOException {
    assertEquals(mapper.readTree(file()), umapper.readTree(file()));
  }

  /** Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#readTree(java.net.URL)}. */
  @Test
  void testReadTreeURL() throws IOException {
    assertEquals(mapper.readTree(url()), umapper.readTree(url()));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#readValues(com.fasterxml.jackson.core.JsonParser,
   * com.fasterxml.jackson.core.type.ResolvedType)}.
   */
  @Test
  void testReadValuesJsonParserResolvedType() throws IOException {
    // not sure how to test at this time
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#readValues(com.fasterxml.jackson.core.JsonParser,
   * com.fasterxml.jackson.databind.JavaType)}.
   */
  @Test
  void testReadValuesJsonParserJavaType() throws IOException {
    Iterable<UncheckedObjectMapperTest.Sample> iterable1 =
        () -> {
          try {
            return mapper.readValues(jsonParser(), type);
          } catch (IOException e) {
            throw new UncheckedIOException(e);
          }
        };
    Iterable<UncheckedObjectMapperTest.Sample> iterable2 =
        () -> umapper.readValues(jsonParser(), type);
    assertIterableEquals(iterable1, iterable2);
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#readValues(com.fasterxml.jackson.core.JsonParser,
   * java.lang.Class)}.
   */
  @Test
  void testReadValuesJsonParserClassOfT() throws IOException {
    Iterable<UncheckedObjectMapperTest.Sample> iterable1 =
        () -> {
          try {
            return mapper.readValues(jsonParser(), Sample.class);
          } catch (IOException e) {
            throw new UncheckedIOException(e);
          }
        };
    Iterable<UncheckedObjectMapperTest.Sample> iterable2 =
        () -> umapper.readValues(jsonParser(), Sample.class);
    assertIterableEquals(iterable1, iterable2);
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#readValues(com.fasterxml.jackson.core.JsonParser,
   * com.fasterxml.jackson.core.type.TypeReference)}.
   */
  @Test
  void testReadValuesJsonParserTypeReferenceOfT() throws IOException {
    Iterable<UncheckedObjectMapperTest.Sample> iterable1 =
        () -> {
          try {
            return mapper.readValues(jsonParser(), ref);
          } catch (IOException e) {
            throw new UncheckedIOException(e);
          }
        };
    Iterable<UncheckedObjectMapperTest.Sample> iterable2 =
        () -> umapper.readValues(jsonParser(), ref);

    assertIterableEquals(iterable1, iterable2);
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#writeTree(com.fasterxml.jackson.core.JsonGenerator,
   * com.fasterxml.jackson.core.TreeNode)}.
   */
  @Test
  void testWriteTreeJsonGeneratorTreeNode() throws IOException {
    ByteArrayOutputStream out1 = new ByteArrayOutputStream();
    ByteArrayOutputStream out2 = new ByteArrayOutputStream();
    JsonGenerator gen1 = new JsonFactory().createGenerator(out1, JsonEncoding.UTF8);
    JsonGenerator gen2 = new JsonFactory().createGenerator(out2, JsonEncoding.UTF8);
    mapper.writeTree(gen1, (TreeNode) node);
    umapper.writeTree(gen2, (TreeNode) node);
    assertArrayEquals(out1.toByteArray(), out2.toByteArray());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#writeTree(com.fasterxml.jackson.core.JsonGenerator,
   * com.fasterxml.jackson.databind.JsonNode)}.
   */
  @Test
  void testWriteTreeJsonGeneratorJsonNode() throws IOException {
    ByteArrayOutputStream out1 = new ByteArrayOutputStream();
    ByteArrayOutputStream out2 = new ByteArrayOutputStream();
    JsonGenerator gen1 = new JsonFactory().createGenerator(out1, JsonEncoding.UTF8);
    JsonGenerator gen2 = new JsonFactory().createGenerator(out2, JsonEncoding.UTF8);
    mapper.writeTree(gen1, node);
    umapper.writeTree(gen2, node);
    assertArrayEquals(out1.toByteArray(), out2.toByteArray());
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#writeValue(java.io.File,
   * java.lang.Object)}.
   */
  @Test
  void testWriteValueFileObject() throws IOException {
    File file1 = file();
    File file2 = file();
    mapper.writeValue(file1, sample);
    umapper.writeValue(file2, sample);
    assertEquals(mapper.readTree(file1), umapper.readTree(file2));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#writeValue(com.fasterxml.jackson.core.JsonGenerator,
   * java.lang.Object)}.
   */
  @Test
  void testWriteValueJsonGeneratorObject() throws IOException {
    ByteArrayOutputStream out1 = new ByteArrayOutputStream();
    ByteArrayOutputStream out2 = new ByteArrayOutputStream();
    JsonGenerator gen1 = new JsonFactory().createGenerator(out1, JsonEncoding.UTF8);
    JsonGenerator gen2 = new JsonFactory().createGenerator(out2, JsonEncoding.UTF8);
    mapper.writeValue(gen1, sample);
    umapper.writeValue(gen2, sample);
    assertArrayEquals(out1.toByteArray(), out2.toByteArray());
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#writeValue(java.io.OutputStream,
   * java.lang.Object)}.
   */
  @Test
  void testWriteValueOutputStreamObject() throws IOException {
    ByteArrayOutputStream out1 = new ByteArrayOutputStream();
    ByteArrayOutputStream out2 = new ByteArrayOutputStream();
    mapper.writeValue(out1, sample);
    umapper.writeValue(out2, sample);
    assertArrayEquals(out1.toByteArray(), out2.toByteArray());
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#writeValue(java.io.DataOutput,
   * java.lang.Object)}.
   */
  @Test
  void testWriteValueDataOutputObject() throws IOException {
    ByteArrayOutputStream out1 = new ByteArrayOutputStream();
    ByteArrayOutputStream out2 = new ByteArrayOutputStream();
    DataOutput data1 = new DataOutputStream(out1);
    DataOutput data2 = new DataOutputStream(out2);
    mapper.writeValue(data1, sample);
    umapper.writeValue(data2, sample);
    assertArrayEquals(out1.toByteArray(), out2.toByteArray());
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#writeValue(java.io.Writer,
   * java.lang.Object)}.
   */
  @Test
  void testWriteValueWriterObject() throws IOException {
    StringWriter out1 = new StringWriter();
    StringWriter out2 = new StringWriter();
    mapper.writeValue(out1, sample);
    umapper.writeValue(out2, sample);
    assertEquals(out1.toString(), out2.toString());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#writeValueAsString(java.lang.Object)}.
   */
  @Test
  void testWriteValueAsStringObject() throws IOException {
    assertEquals(mapper.writeValueAsString(sample), umapper.writeValueAsString(sample));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#writeValueAsBytes(java.lang.Object)}.
   */
  @Test
  void testWriteValueAsBytesObject() throws IOException {
    assertArrayEquals(mapper.writeValueAsBytes(sample), umapper.writeValueAsBytes(sample));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#treeToValue(com.fasterxml.jackson.core.TreeNode,
   * java.lang.Class)}.
   */
  @Test
  void testTreeToValueTreeNodeClassOfT() throws IOException {
    assertEquals(
        mapper.treeToValue((TreeNode) node, Sample.class),
        umapper.treeToValue((TreeNode) node, Sample.class));
  }

  /**
   * Test method for {@link edu.byu.hbll.json.UncheckedObjectMapper#updateValue(java.lang.Object,
   * java.lang.Object)}.
   */
  @Test
  void testUpdateValueTObject() throws IOException {
    umapper.updateValue(sample, new Sample("2", 2));
    assertEquals("2", sample.getA());
    assertEquals(2, sample.getB());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#acceptJsonFormatVisitor(java.lang.Class,
   * com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatVisitorWrapper)}.
   */
  @Test
  void testAcceptJsonFormatVisitorClassOfQJsonFormatVisitorWrapper() throws IOException {
    // not sure how to test other than just calling it
    umapper.acceptJsonFormatVisitor(Sample.class, new JsonFormatVisitorWrapper.Base());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.json.UncheckedObjectMapper#acceptJsonFormatVisitor(com.fasterxml.jackson.databind.JavaType,
   * com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatVisitorWrapper)}.
   */
  @Test
  void testAcceptJsonFormatVisitorJavaTypeJsonFormatVisitorWrapper() throws IOException {
    // not sure how to test other than just calling it
    umapper.acceptJsonFormatVisitor(type, new JsonFormatVisitorWrapper.Base());
  }

  private DataInput dataInput() {
    return new DataInputStream(new ByteArrayInputStream(bytes));
  }

  private File file() {
    try {
      File file = Files.createTempFile("", "").toFile();
      file.deleteOnExit();
      Files.write(file.toPath(), bytes);
      return file;
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private URL url() {
    try {
      return file().toURI().toURL();
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    }
  }

  private Reader reader() {
    return new StringReader(text);
  }

  private InputStream inputStream() {
    return new ByteArrayInputStream(bytes);
  }

  private JsonParser jsonParser() {
    try {
      return new JsonFactory().createParser(bytes);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  private static class Sample {
    private String a;
    private int b;
  }
}
