package edu.byu.hbll.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.jupiter.api.Test;

/**
 * Unit tests (and a couple integration tests that read from a yaml file included with this project)
 * for YamlLoader.
 */
public class YamlLoaderTest {

  private static String fullSource1 = readFile("fullSource1.yml");
  private static String fullSource2 = readFile("fullSource2.yml");
  private static String fullSource3 = readFile("fullSource3.yml");

  private static Path sourcePath = Paths.get("src/test/resources/test.yml");

  private static YamlLoader loader = new YamlLoader();
  private static ObjectMapper mapper = new ObjectMapper();

  /**
   * Reads a file from src/test/resources to a string
   *
   * @param name the filename
   * @return the contents of the file
   */
  private static String readFile(String name) {
    try {
      return new String(
          Files.readAllBytes(Paths.get("src/test/resources", name)), StandardCharsets.UTF_8);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /**
   * The load(Reader...readers) method should fail with a NullPointerException if only null values
   * are provided.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void load_shouldFailIfNoReadersProvided() throws Exception {
    assertThrows(NullPointerException.class, () -> loader.load((Reader) null));
  }

  /**
   * The load(Reader...readers) method should fail with a NullPointerException if any null values
   * are provided.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void load_shouldFailIfAnyNullReadersProvided() throws Exception {
    assertThrows(
        NullPointerException.class,
        () ->
            loader.load(
                new StringReader(fullSource1), (Reader) null, new StringReader(fullSource2)));
  }

  /**
   * The load(Reader...readers) method should return the appropriate JsonNode for a single source.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void load_shouldProperlyBuildJsonNodeFromSingleReader() throws Exception {
    JsonNode node = loader.load(new StringReader(fullSource1));

    assertEquals(1, node.path("intVar").asInt());
    assertEquals("value", node.path("strVar").asText());
    assertEquals("childValue", node.path("objVar").path("objChild").asText());
  }

  /**
   * The load(Reader...readers) method should return the appropriate JsonNode for a multiple
   * sources. The specific logic for merging the data from multiple nodes is tested in the merge_*
   * test methods.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void load_shouldBuildJsonNodeFromMultipleReader() throws Exception {
    JsonNode node =
        loader.load(
            new StringReader(fullSource1),
            new StringReader(fullSource2),
            new StringReader(fullSource3));

    assertEquals(1, node.path("intVar").asInt());
    assertEquals("updatedStrValue", node.path("strVar").asText());
    assertEquals("childValue", node.path("objVar").path("objChild").asText());
    assertEquals("newChildValue", node.path("objVar").path("newChild").asText());
    assertEquals("newValue", node.path("newVar").asText());
  }

  /**
   * The load(Class targetType, Reader...readers) should return a populated target POJO rather than
   * a raw JsonNode.
   *
   * <p>Only the target/POJO piece of this method is tested here, since the previous tests have
   * complete coverage on the underlying logic.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void load_shouldProperlyBuildTargetClassFromReaders() throws Exception {
    ExampleTarget target = loader.load(ExampleTarget.class, new StringReader(fullSource1));

    assertEquals(Integer.valueOf(1), target.intVar);
    assertEquals("value", target.strVar);
    assertEquals("childValue", target.objVar.objChild);
  }

  /**
   * The load(Path...paths) should return the appropriate JsonNode for one or more paths.
   *
   * <p>Only the use of Path instead of Reader is tested here, since the previous tests have
   * complete coverage on the underlying logic.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void load_shouldProperlyBuildJsonNodeFromPaths() throws Exception {
    JsonNode node = loader.load(sourcePath);

    assertEquals(1, node.path("intVar").asInt());
    assertEquals("value", node.path("strVar").asText());
    assertEquals("childValue", node.path("objVar").path("objChild").asText());
  }

  /**
   * The load(Class targetType, Path...paths) should return a populated target POJO for one or more
   * paths.
   *
   * <p>Only the use of Path instead of Reader is tested here, since the previous tests have
   * complete coverage on the underlying logic.
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void load_shouldProperlyBuildTargetClassFromPaths() throws Exception {
    ExampleTarget target = loader.load(ExampleTarget.class, sourcePath);

    assertEquals(Integer.valueOf(1), target.intVar);
    assertEquals("value", target.strVar);
    assertEquals("childValue", target.objVar.objChild);
  }

  /**
   * Verifies that yaml files and their imports are loaded with correct precedence levels. The test
   * yaml files are set up to be loaded in alphabetical order (lowest to highest precedence).
   *
   * @throws Exception if any exception occurs while running the test
   */
  @Test
  public void testImports() throws Exception {
    JsonNode node =
        loader.load(
            Paths.get("src/test/resources/import/c.yml"),
            Paths.get("src/test/resources/import/f.yml"));

    for (char c = 'a'; c <= 'f'; c++) {
      // each value should equal its key name
      assertEquals(c + "", node.path(c + "").asText());
    }
  }

  /**
   * Tests advanced YAML features and syntax.
   *
   * @throws Exception
   */
  @Test
  public void testAdvanced() throws Exception {
    JsonNode expected = mapper.readTree(new File("src/test/resources/advanced.json"));
    JsonNode actual = loader.load(Paths.get("src/test/resources/advanced.yml"));
    assertEquals(expected, actual);
  }

  /** Example target bean for testing the ability of YamlLoader to parse YAML into a POJO. */
  public static class ExampleTarget {

    @JsonProperty Integer intVar;
    @JsonProperty String strVar;
    @JsonProperty ObjVar objVar;

    public static class ObjVar {

      @JsonProperty String objChild;
    }
  }

  /**
   * Tests the basic functionality of loadPaths.
   *
   * @throws Exception
   */
  @Test
  public void testLoadPaths() throws Exception {
    String path =
        "src/test/resources/fullSource1.yml"
            + File.pathSeparator
            + "src/test/resources/fullSource2.yml"
            + File.pathSeparator
            + "src/test/resources/fullSource3.yml";

    JsonNode node = loader.loadPaths(path);

    assertEquals(1, node.path("intVar").asInt());
    assertEquals("updatedStrValue", node.path("strVar").asText());
    assertEquals("childValue", node.path("objVar").path("objChild").asText());
    assertEquals("newChildValue", node.path("objVar").path("newChild").asText());
    assertEquals("newValue", node.path("newVar").asText());
  }

  /**
   * This is mostly tested by all the other tests. Here we're mostly testing the inner object
   * mapper.
   */
  @Test
  public void testYamlLoader() throws Exception {
    Reader r = new StringReader("a: null");
    JsonNode node = new YamlLoader().load(r);
    // a: null should still be there
    assertTrue(node.has("a"));
  }

  /** Test setting the object mapper. */
  @Test
  public void testYamlLoaderObjectMapper() throws Exception {
    Reader r = new StringReader("a: null");
    JsonNode node = new YamlLoader(ObjectMapperFactory.newUnchecked()).load(r);
    // `a: null` should NOT be there with the unchecked mapper
    assertFalse(node.has("a"));
  }
}
