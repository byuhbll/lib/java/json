package edu.byu.hbll.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Unit tests for {@link ObjectMapperFactory}. */
public class ObjectMapperFactoryTest {

  private String json = "{\"key\":\"value\"}";

  private ObjectMapper checked = ObjectMapperFactory.newChecked();
  private ObjectMapper unchecked = ObjectMapperFactory.newUnchecked();

  /**
   * Verifies that calling {@link ObjectMapperFactory#newDefault()} will return an {@link
   * ObjectMapper} which will successfully read a simple JSON object.
   */
  @Test
  public void newDefaultShouldReturnFunctioningObjectMapper() {
    @SuppressWarnings("deprecation")
    JsonNode jsonNode = ObjectMapperFactory.newDefault().readTree(json);
    assertEquals("value", jsonNode.path("key").asText());

    try {
      assertEquals("value", ObjectMapperFactory.newChecked().readTree(json).path("key").asText());
    } catch (IOException e) {
      // and throws checked io exception
      throw new UncheckedIOException(e);
    }

    assertEquals("value", ObjectMapperFactory.newUnchecked().readTree(json).path("key").asText());
  }

  /**
   * Verifies that calling {@link ObjectMapperFactory#newDefault()} will return a new {@link
   * ObjectMapper} reference each time it is called, so that future mutations of the {@link
   * ObjectMapper} can safely be made.
   */
  @SuppressWarnings("deprecation")
  @Test
  public void newDefaultShouldReturnUniqueObjectMapper() {
    assertFalse(ObjectMapperFactory.newDefault() == ObjectMapperFactory.newDefault());
    assertFalse(ObjectMapperFactory.newChecked() == ObjectMapperFactory.newChecked());
    assertFalse(ObjectMapperFactory.newUnchecked() == ObjectMapperFactory.newUnchecked());
  }

  /**
   * Tests the {@link ObjectMapper} is properly configured.
   *
   * @throws Exception
   */
  @Test
  public void testAcceptSingleValueAsArray() throws Exception {
    for (ObjectMapper mapper : Arrays.asList(checked, unchecked)) {
      // tests DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY
      TestObject o = mapper.readValue("{\"array\":\"one\"}", TestObject.class);
      assertEquals("one", o.array.get(0));
    }
  }

  /**
   * Tests the {@link ObjectMapper} is properly configured.
   *
   * @throws Exception
   */
  @Test
  public void testFailOnUnknownProperties() throws Exception {
    for (ObjectMapper mapper : Arrays.asList(checked, unchecked)) {
      // tests DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
      mapper.readValue("{\"unknown\":\"property\"}", TestObject.class);
    }
  }

  /**
   * Tests the {@link ObjectMapper} is properly configured.
   *
   * @throws Exception
   */
  @Test
  public void testWriteDatesAsTimestamps() throws Exception {
    for (ObjectMapper mapper : Arrays.asList(checked, unchecked)) {
      // tests SerializationFeature.WRITE_DATES_AS_TIMESTAMPS
      assertEquals(
          "{\"date\":\"1970-01-01T00:00:00.000+00:00\"}",
          mapper.writeValueAsString(new TestObject()));
    }
  }

  /**
   * Tests the {@link ObjectMapper} is properly configured.
   *
   * @throws Exception
   */
  @Test
  public void testJavaTimeModule() throws Exception {
    for (ObjectMapper mapper : Arrays.asList(checked, unchecked)) {
      // tests new JavaTimeModule()
      TestObject o = mapper.readValue("{\"duration\":\"PT0S\"}", TestObject.class);
      assertEquals(Duration.ZERO, o.duration);
    }
  }

  public static class TestObject {
    public List<String> array;
    public Date date = Date.from(Instant.EPOCH);
    public Duration duration;
  }
}
