package edu.byu.hbll.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.type.ResolvedType;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.MappingJsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.DefaultDeserializationContext;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatVisitorWrapper;
import com.fasterxml.jackson.databind.ser.BeanSerializerFactory;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.net.URL;

/**
 * UncheckedObjectMapper extends the Jackson {@link ObjectMapper} class in such a way that the
 * functionality is identical except that all {@link IOException}s are wrapped and thrown as {@link
 * UncheckedIOException}s. All public methods and constructors from {@link ObjectMapper} are
 * available to users of this class as well.
 *
 * <p>This class is therefore much easier to use with Optionals, Streams, and other lambda-friendly
 * objects introduced in Java 8. The user retains responsibility for handling parsing exceptions in
 * cases where the JSON data could fail to serialize or deserialize.
 */
public class UncheckedObjectMapper extends ObjectMapper {

  private static final long serialVersionUID = 1L;

  /**
   * Default construction, which will construct the default {@link JsonFactory} as necessary, use
   * {@link SerializerProvider} as its {@link SerializerProvider}, and {@link BeanSerializerFactory}
   * as its {@link SerializerFactory}.
   */
  public UncheckedObjectMapper() {
    super();
  }

  /**
   * Constructs instance that uses specified {@link JsonFactory} for constructing necessary {@link
   * JsonParser}s and/or {@link JsonGenerator}s.
   *
   * @param jf JsonFactory to use
   */
  public UncheckedObjectMapper(JsonFactory jf) {
    super(jf);
  }

  /**
   * Constructs instance that uses specified {@link JsonFactory} for constructing necessary {@link
   * JsonParser}s and/or {@link JsonGenerator}s, and uses given providers for accessing serializers
   * and deserializers.
   *
   * @param jf JsonFactory to use; if null, a new {@link MappingJsonFactory} will be constructed
   * @param sp SerializerProvider to use; if null, a {@link SerializerProvider} will be constructed
   * @param dc blueprint deserialization context instance to user for creating actual context
   *     objects; if null, will construct standard {@link DeserializationContext}.
   */
  public UncheckedObjectMapper(
      JsonFactory jf, DefaultSerializerProvider sp, DefaultDeserializationContext dc) {
    super(jf, sp, dc);
  }

  /**
   * Copy-constructor to construct a new instance of this class using a standard {@link
   * ObjectMapper} as a template.
   *
   * @param src the original {@link ObjectMapper}
   */
  public UncheckedObjectMapper(ObjectMapper src) {
    super(src);
  }

  @Override
  public ObjectMapper copy() {
    _checkInvalidCopy(UncheckedObjectMapper.class);
    return new UncheckedObjectMapper(this);
  }

  @Override
  public <T> T readValue(byte[] src, Class<T> valueType) {
    try {
      return super.readValue(src, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(byte[] src, int offset, int len, Class<T> valueType) {
    try {
      return super.readValue(src, offset, len, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(byte[] src, TypeReference<T> valueTypeRef) {
    try {
      return super.readValue(src, valueTypeRef);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(byte[] src, int offset, int len, TypeReference<T> valueTypeRef) {
    try {
      return super.readValue(src, offset, len, valueTypeRef);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(byte[] src, JavaType valueType) {
    try {
      return super.readValue(src, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(byte[] src, int offset, int len, JavaType valueType) {
    try {
      return super.readValue(src, offset, len, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(DataInput src, Class<T> valueType) {
    try {
      return super.readValue(src, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(DataInput src, JavaType valueType) {
    try {
      return super.readValue(src, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(File src, Class<T> valueType) {
    try {
      return super.readValue(src, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(File src, TypeReference<T> valueTypeRef) {
    try {
      return super.readValue(src, valueTypeRef);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(File src, JavaType valueType) {
    try {
      return super.readValue(src, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(URL src, Class<T> valueType) {
    try {
      return super.readValue(src, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(URL src, TypeReference<T> valueTypeRef) {
    try {
      return super.readValue(src, valueTypeRef);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(URL src, JavaType valueType) {
    try {
      return super.readValue(src, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(String content, Class<T> valueType) {
    try {
      return super.readValue(content, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(String content, TypeReference<T> valueTypeRef) {
    try {
      return super.readValue(content, valueTypeRef);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(String content, JavaType valueType) {
    try {
      return super.readValue(content, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(Reader src, Class<T> valueType) {
    try {
      return super.readValue(src, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(Reader src, TypeReference<T> valueTypeRef) {
    try {
      return super.readValue(src, valueTypeRef);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(Reader src, JavaType valueType) {
    try {
      return super.readValue(src, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(InputStream src, Class<T> valueType) {
    try {
      return super.readValue(src, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(InputStream src, TypeReference<T> valueTypeRef) {
    try {
      return super.readValue(src, valueTypeRef);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(InputStream src, JavaType valueType) {
    try {
      return super.readValue(src, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(JsonParser p, Class<T> valueType) {
    try {
      return super.readValue(p, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(JsonParser p, TypeReference<T> valueTypeRef) {
    try {
      return super.readValue(p, valueTypeRef);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T readValue(JsonParser p, JavaType valueType) {
    try {
      return super.readValue(p, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T extends TreeNode> T readTree(JsonParser p) {
    try {
      return super.readTree(p);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public JsonNode readTree(InputStream in) {
    try {
      return super.readTree(in);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public JsonNode readTree(Reader r) {
    try {
      return super.readTree(r);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public JsonNode readTree(String content) {
    try {
      return super.readTree(content);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public JsonNode readTree(byte[] content) {
    try {
      return super.readTree(content);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public JsonNode readTree(byte[] content, int offset, int len) {
    try {
      return super.readTree(content, offset, len);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public JsonNode readTree(File file) {
    try {
      return super.readTree(file);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public JsonNode readTree(URL source) {
    try {
      return super.readTree(source);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> MappingIterator<T> readValues(JsonParser p, ResolvedType valueType) {
    try {
      return super.readValues(p, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> MappingIterator<T> readValues(JsonParser p, JavaType valueType) {
    try {
      return super.readValues(p, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> MappingIterator<T> readValues(JsonParser p, Class<T> valueType) {
    try {
      return super.readValues(p, valueType);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> MappingIterator<T> readValues(JsonParser p, TypeReference<T> valueTypeRef) {
    try {
      return super.readValues(p, valueTypeRef);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public void writeTree(JsonGenerator g, TreeNode rootNode) {
    try {
      super.writeTree(g, rootNode);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public void writeTree(JsonGenerator g, JsonNode rootNode) {
    try {
      super.writeTree(g, rootNode);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public void writeValue(File resultFile, Object value) {
    try {
      super.writeValue(resultFile, value);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public void writeValue(JsonGenerator g, Object value) {
    try {
      super.writeValue(g, value);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public void writeValue(OutputStream out, Object value) {
    try {
      super.writeValue(out, value);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public void writeValue(DataOutput out, Object value) {
    try {
      super.writeValue(out, value);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public void writeValue(Writer w, Object value) {
    try {
      super.writeValue(w, value);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public String writeValueAsString(Object value) {
    try {
      return super.writeValueAsString(value);
    } catch (JsonProcessingException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public byte[] writeValueAsBytes(Object value) {
    try {
      return super.writeValueAsBytes(value);
    } catch (JsonProcessingException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T treeToValue(TreeNode n, Class<T> valueType) {
    try {
      return super.treeToValue(n, valueType);
    } catch (JsonProcessingException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public <T> T updateValue(T valueToUpdate, Object overrides) {
    try {
      return super.updateValue(valueToUpdate, overrides);
    } catch (JsonProcessingException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public void acceptJsonFormatVisitor(Class<?> type, JsonFormatVisitorWrapper visitor) {
    try {
      super.acceptJsonFormatVisitor(type, visitor);
    } catch (JsonProcessingException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public void acceptJsonFormatVisitor(JavaType type, JsonFormatVisitorWrapper visitor) {
    try {
      super.acceptJsonFormatVisitor(type, visitor);
    } catch (JsonProcessingException e) {
      throw new UncheckedIOException(e);
    }
  }
}
