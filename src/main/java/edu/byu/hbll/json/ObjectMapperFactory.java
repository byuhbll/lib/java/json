package edu.byu.hbll.json;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Optional;

/**
 * Utility object for creating pre-configured instances of {@link ObjectMapper} suitable for most
 * purposes.
 */
public final class ObjectMapperFactory {

  /**
   * Constructs a new instance; since this is a utility class, this constructor should never be
   * called.
   */
  private ObjectMapperFactory() {
    // Do nothing.
  }

  @Deprecated
  private static final ObjectMapper DEFAULT_TEMPLATE =
      new UncheckedObjectMapper()
          .configure(SerializationFeature.INDENT_OUTPUT, true)
          .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
          .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
          .enable(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME)
          .registerModule(new JavaTimeModule())
          .registerModule(new Jdk8Module().configureAbsentsAsNulls(true))
          .setSerializationInclusion(Include.NON_ABSENT);

  private static final ObjectMapper DEFAULT_CHECKED_TEMPLATE =
      new ObjectMapper()
          .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
          .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
          .enable(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME)
          .registerModule(new JavaTimeModule())
          .registerModule(new Jdk8Module().configureAbsentsAsNulls(true))
          .setSerializationInclusion(Include.NON_ABSENT)
          .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);

  private static final UncheckedObjectMapper DEFAULT_UNCHECKED_TEMPLATE =
      new UncheckedObjectMapper(DEFAULT_CHECKED_TEMPLATE);

  /**
   * Returns a default {@link ObjectMapper} suitable for almost all purposes.
   *
   * <p>This {@link ObjectMapper} will automatically indent/prettify output, will NOT fail on
   * unknown properties, properly handles java.time date and time objects, and properly handles Java
   * 8 objects such as {@link Optional}. Additionally, since the returned reference is actually an
   * {@link UncheckedObjectMapper}, it will throw {@link UncheckedIOException}s instead of the
   * checked {@link IOException}, making it easier to use with lambdas.
   *
   * @return new {@link ObjectMapper} instance configured as described above
   * @deprecated use {@link #newUnchecked()} instead.
   */
  @Deprecated
  public static UncheckedObjectMapper newDefault() {
    return (UncheckedObjectMapper) DEFAULT_TEMPLATE.copy();
  }

  /**
   * Returns a default {@link ObjectMapper} suitable for almost all purposes.
   *
   * <p>This {@link ObjectMapper} will NOT fail on unknown properties, properly handles java.time
   * date and time objects, properly handles Java 8 objects such as {@link Optional}, and can
   * deserialize a non-array value to a list of size one.
   *
   * @return new {@link ObjectMapper} instance configured as described above
   */
  public static ObjectMapper newChecked() {
    return DEFAULT_CHECKED_TEMPLATE.copy();
  }

  /**
   * Returns a default {@link UncheckedObjectMapper} suitable for almost all purposes.
   *
   * <p>This {@link ObjectMapper} will NOT fail on unknown properties, properly handles java.time
   * date and time objects, properly handles Java 8 objects such as {@link Optional}, and can
   * deserialize a non-array value to a list of size one. Additionally, since the returned reference
   * is actually an {@link UncheckedObjectMapper}, it will throw {@link UncheckedIOException}s
   * instead of the checked {@link IOException}, making it easier to use with lambdas.
   *
   * @return new {@link UncheckedObjectMapper} instance configured as described above
   */
  public static UncheckedObjectMapper newUnchecked() {
    return (UncheckedObjectMapper) DEFAULT_UNCHECKED_TEMPLATE.copy();
  }
}
