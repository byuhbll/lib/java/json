package edu.byu.hbll.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.regex.Pattern;
import org.yaml.snakeyaml.Yaml;

/**
 * Loads YAML data into JsonNodes and/or target POJOs.
 *
 * <p>This loader uses Jackson's YAML plugin to do the actual parsing of data. As such, this class
 * follows Jackson's own recommendations regarding thread-safeness; that is that instances of this
 * class are fully thread-safe provided that ALL configuration of the internal ObjectMapper occurs
 * before any loading/deserialization takes place.
 *
 * <p>YAML documents containing an \@import field as an array of file paths will recursively import
 * the YAML documents found at those paths giving precedence to the YAML document doing the import.
 */
public class YamlLoader {

  private final ObjectMapper mapper;

  /**
   * We use SnakeYAML directly vs `new ObjectMapper(new YAMLFactory())` because the latter does not
   * support all advanced YAML features and syntax.
   */
  private Yaml yaml = new Yaml();

  /**
   * Creates a new {@link YamlLoader} with an internal {@link ObjectMapper} that will not fail on
   * unknown properties if binding to POJOs.
   */
  public YamlLoader() {
    this(new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false));
  }

  /**
   * Creates a new {@link YamlLoader} with the given {@link ObjectMapper} to be used when binding to
   * POJOs.
   *
   * @param mapper the mapper to use internally
   */
  public YamlLoader(ObjectMapper mapper) {
    this.mapper = mapper;
  }

  /**
   * Deserializes the YAML data contained in the provided files. File paths should be separated by a
   * platform dependent delimiter. For Windows that is ';' and all others use ':'--similar to the
   * PATH variables on each. If more than one file is provided, then the first will be used to
   * established a baseline node; subsequent files will merge into that baseline node in sequence,
   * adding or overwriting fields as appropriate in a cascading fashion.
   *
   * @param path a platform dependent delimited list of file paths denoting the files containing the
   *     raw YAML data
   * @return the merged JsonNode
   * @throws JsonProcessingException if a failure occurs while parsing the JSON data
   * @throws IOException if reading one or more of the file paths fails
   */
  public JsonNode loadPaths(String path) throws JsonProcessingException, IOException {
    return loadPaths(path, File.pathSeparator);
  }

  /**
   * Deserializes the YAML data contained in the provided files. File paths should be separated by a
   * platform dependent delimiter. For Windows that is ';' and all others use ':'--similar to the
   * PATH variables on each. If more than one file is provided, then the first will be used to
   * established a baseline node; subsequent files will merge into that baseline node in sequence,
   * adding or overwriting fields as appropriate in a cascading fashion.
   *
   * @param path a platform dependent delimited list of file paths denoting the files containing the
   *     raw YAML data
   * @param delimiter the delimiter that separates paths
   * @return the merged JsonNode
   * @throws JsonProcessingException if a failure occurs while parsing the JSON data
   * @throws IOException if reading one or more of the file paths fails
   */
  public JsonNode loadPaths(String path, String delimiter)
      throws JsonProcessingException, IOException {
    Path[] pathsArray =
        Arrays.stream(path.split(Pattern.quote(delimiter)))
            .map(Paths::get)
            .toArray(size -> new Path[size]);
    return load(pathsArray);
  }

  /**
   * Deserializes the YAML data contained in the provided readers into a single JsonNode. If more
   * than one reader is provided, then the first reader will be used to established a baseline node;
   * subsequent readers will merge into that baseline node in sequence, adding or overwriting fields
   * as appropriate in a cascading fashion.
   *
   * @param readers the readers containing the raw YAML data
   * @return the merged JsonNode
   * @throws JsonProcessingException if a failure occurs while parsing the JSON data
   * @throws IOException if one or more of the readers fails
   */
  public JsonNode load(Reader... readers) throws JsonProcessingException, IOException {
    return load(null, Paths.get(""), readers);
  }

  /**
   * Deserializes the YAML data contained in the provided readers into the provided Jackson POJO. If
   * more than one reader is provided, then the first reader will be used to established a baseline
   * JsonNode; subsequent readers will merge into that baseline node in sequence, adding or
   * overwriting fields as appropriate in a cascading fashion before the final boxing of the data
   * into the target POJO type.
   *
   * @param targetType deserialize data into a new instance of this type
   * @param readers the readers containing the raw YAML data
   * @param <T> targetType type
   * @return the target object containing the parsed data
   * @throws JsonProcessingException if a failure occurs while parsing the JSON data or while boxing
   *     the result
   * @throws IOException if one or more of the readers fails
   */
  public <T> T load(Class<T> targetType, Reader... readers)
      throws JsonProcessingException, IOException {
    return mapper.treeToValue(load(readers), targetType);
  }

  /**
   * Deserializes the YAML data contained in the provided files into a single JsonNode. If more than
   * one file is provided, then the first file will be used to established a baseline node;
   * subsequent files will merge into that baseline node in sequence, adding or overwriting fields
   * as appropriate in a cascading fashion.
   *
   * @param paths the files containing the raw YAML data
   * @return the merged JsonNode
   * @throws JsonProcessingException if a failure occurs while parsing the JSON data
   * @throws IOException if a failure occurs while reading data from one or more of the files
   */
  public JsonNode load(Path... paths) throws JsonProcessingException, IOException {
    JsonNode mainNode = null;

    for (Path path : paths) {
      try (Reader reader = Files.newBufferedReader(path)) {
        mainNode = load(mainNode, path.getParent(), reader);
      }
    }

    return mainNode;
  }

  /**
   * Deserializes the YAML data contained in the provided files into the provided Jackson POJO. If
   * more than one file is provided, then the first file will be used to established a baseline
   * JsonNode; subsequent files will merge into that baseline node in sequence, adding or
   * overwriting fields as appropriate in a cascading fashion before the final boxing of the data
   * into the target POJO type.
   *
   * @param targetType deserialize data into a new instance of this type
   * @param paths the files containing the raw YAML data
   * @param <T> targetType type
   * @return the target object containing the parsed data
   * @throws JsonProcessingException if a failure occurs while parsing the JSON data or while boxing
   *     the result
   * @throws IOException if a failure occurs while reading data from one or more of the files
   */
  public <T> T load(Class<T> targetType, Path... paths)
      throws JsonProcessingException, IOException {
    return mapper.treeToValue(load(paths), targetType);
  }

  /**
   * Deserializes the YAML data contained in the provided readers into a single JsonNode. If more
   * than one reader is provided, then the first reader will be used to established a baseline node;
   * subsequent readers will merge into that baseline node in sequence, adding or overwriting fields
   * as appropriate in a cascading fashion. Any YAML documents containing an \@import field as an
   * array of file paths will recursively import the YAML documents found at those paths giving
   * precedence to the YAML document doing the import.
   *
   * @param mainNode the base json node to receive updates, use null if starting empty
   * @param directory the directory used for constructing relative paths
   * @param readers the readers containing the raw YAML data
   * @return the merged JsonNode
   * @throws JsonProcessingException if a failure occurs while parsing the JSON data
   * @throws IOException if one or more of the readers fails
   */
  private JsonNode load(JsonNode mainNode, Path directory, Reader... readers)
      throws JsonProcessingException, IOException {
    for (Reader reader : readers) {
      JsonNode thisNode = mapper.valueToTree(yaml.load(reader));

      for (JsonNode importField : thisNode.path("@import")) {
        JsonNode updateNode = load(directory.resolve(importField.asText()));
        mainNode = JsonUtils.merge(mainNode, updateNode);
      }

      mainNode = JsonUtils.merge(mainNode, thisNode);
    }

    return mainNode;
  }

  /**
   * Returns the ObjectMapper used to load/deserialize YAML files. Clients may modify this
   * ObjectMapper to meet their specific needs.
   *
   * @return the mapper
   */
  public ObjectMapper getObjectMapper() {
    return mapper;
  }
}
