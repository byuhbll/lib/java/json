package edu.byu.hbll.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Static utility methods for working with {@link JsonNode}s.
 *
 * @author Charles Draper
 */
public class JsonUtils {

  private static final ObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  /**
   * Transforms {@link ObjectNode}s with array indexes as keys into arrays, recursively.
   *
   * <p>When using yaml with spring configuration, spring turns lists in yaml into a map where the
   * keys are the indexes of the list. This method recurses through all children of a {@link
   * JsonNode}, checking for {@link ObjectNode}s that have list indexes for keys and converting them
   * to {@code ArrayNode}s.
   *
   * @param node the JsonNode to check for arrays.
   */
  public static void fixArrays(JsonNode node) {
    if (node.isObject()) {
      List<String> childNames = new ArrayList<>();
      node.fieldNames().forEachRemaining(childNames::add);

      for (String childName : childNames) {
        JsonNode child = node.path(childName);

        if (child.isObject()) {
          boolean isArray = true;

          for (int i = 0; i < child.size(); i++) {
            if (!child.has(i + "")) {
              isArray = false;
              break;
            }
          }

          if (isArray) {
            ArrayNode array = ((ObjectNode) node).putArray(childName);
            child.forEach(c -> array.add(c));
          }
        }
      }
    }

    node.forEach(n -> fixArrays(n));
  }

  /**
   * Merges data right to left from the given {@link JsonNode}'s and returns a newly merged copy of
   * the data. Nodes to the right take precedence.
   *
   * <p>Note that recursive merging only occurs when merging {@link ObjectNode}'s. Other node types
   * such as arrays, value nodes, and null nodes will completely overwrite any prior values. {@link
   * MissingNode}'s and null values (not to be confused with the {@link NullNode} type) are ignored.
   *
   * <p>Derived from a method contributed to http://stackoverflow.com/a/11459962 by StackOverflow
   * users arne (http://stackoverflow.com/users/838776/arne) and c4k
   * (http://stackoverflow.com/users/1259118/c4k).
   *
   * @param nodes the nodes to merge
   * @return the merged node or an empty object node if no viable candidates in node list
   */
  public static JsonNode merge(JsonNode... nodes) {
    JsonNode mainNode = mapper.createObjectNode();

    if (nodes == null) {
      return mainNode;
    }

    for (JsonNode updateNode : nodes) {
      if (updateNode != null) {
        mainNode = recursiveMerge(mainNode, updateNode.deepCopy());
      }
    }

    return mainNode;
  }

  /**
   * Merges data from the second argument into the JsonNode provided as the first argument.
   *
   * <p>Note that recursive merging only occurs when merging {@link ObjectNode}'s. Other node types
   * such as arrays, value nodes, and null nodes will completely overwrite any prior values. {@link
   * MissingNode}'s and null values (not to be confused with the {@link NullNode} type) are ignored.
   *
   * <p>Derived from a method contributed to http://stackoverflow.com/a/11459962 by StackOverflow
   * users arne (http://stackoverflow.com/users/838776/arne) and c4k
   * (http://stackoverflow.com/users/1259118/c4k).
   *
   * @param mainNode the node containing the baseline, original data
   * @param updateNode the node containing new data to merge
   */
  private static JsonNode recursiveMerge(JsonNode mainNode, JsonNode updateNode) {

    if (updateNode == null || updateNode.isMissingNode()) {
      return mainNode;
    } else if (mainNode == null || !mainNode.isObject() || !updateNode.isObject()) {
      return updateNode;
    }

    // Update node is an object
    Iterator<String> fieldNames = updateNode.fieldNames();
    while (fieldNames.hasNext()) {
      String fieldName = fieldNames.next();
      JsonNode currentMainNode = mainNode.path(fieldName);
      JsonNode currentUpdateNode = updateNode.path(fieldName);
      if (currentMainNode.isObject() && currentUpdateNode.isObject()) {
        // Recursive case
        recursiveMerge(currentMainNode, currentUpdateNode);
      } else if (mainNode.isObject()) {
        // Base Case
        ((ObjectNode) mainNode).replace(fieldName, currentUpdateNode);
      }
    }
    return mainNode;
  }
}
